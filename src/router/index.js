import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Plataforma from '../views/Plataforma.vue'
import Detalle from '../views/Detalle.vue'
import Form from '../components/Form.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/plataforma',
    name: 'plataforma',
    component: Plataforma
  },
  {
    path: '/detalle',
    name: 'detalle',
    component: Detalle
  },
  {
    path: '/form',
    name: 'form',
    component: Form
  },
]

const router = new VueRouter({
  routes
})

export default router
